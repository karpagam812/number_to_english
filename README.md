# Converting number to english word

This django application exposes 2 endpoint get and post to do the following\
This application tries to convert the number received from post into english number [eg 1 to one] without using any existing function like num2word. One can find the logic of the progrom in serializers.py inside numWordapp file.

## Setup

The first thing to do is to clone the repository:

```sh
$ git clone https://gitlab.com/karpagam812/number_to_english.git
$ cd app
$ python manage.py runserver
```

You can also create virtual env and run in it

The application exposes 2 endpoint 

## GET
Get num_to_english/get?number=1234567

If number is specified in the get request and number exists, the get request will return the following\
\
\
HTTP 200 OK\
Allow: OPTIONS, GET\
Content-Type: application/json\
Vary: Accept\
\
{\
    "number": "11",\
    "status": "ok",\
    "num_in_english": "eleven"\
}

If the number does not exist or not specified in the get request, then it will return everything in the database

## POST
POST num_to_english/

{\
“number”: “12345678” \
}

This endpoints will convert any number given to it into the english words that describe that number. For example the above request should return:\
{\
    "number": "12345678"\
    “status”: “ok”,\
    “num_in_english”: “twelve million three hundred forty five thousand six hundred seventy eight” \
}

Status is reserved for messaging back if the process succeeded or failed. 

## Tests

To run the tests, `cd` into the directory where `manage.py` is:
```sh
(env)$ ./manage.py test
```


