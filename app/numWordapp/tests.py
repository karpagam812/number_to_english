from django.test import TestCase
from .models import Data
from .serializers import DataSerializer

# Create your tests here.
class DataTestCase(TestCase):
    def setUp(self):
        Data.objects.create(number="1")
        Data.objects.create(number="nan")

    def test_word_in_english_ok(self):
        """Number in Engligh"""
        one = Data.objects.get(number="1")
        self.assertEqual(DataSerializer(one).data['num_in_english'], 'one')
    
    def test_word_in_english_failed(self):
        """Number in Engligh"""
        characters = Data.objects.get(number="nan")
        self.assertEqual(DataSerializer(characters).data['num_in_english'], 'Not a number')

    def test_status_failed(self):
        "Failed status check"
        characters = Data.objects.get(number="nan")
        self.assertEqual(DataSerializer(characters).data['status'], 'Failed')

    def test_status_ok(self):
        "Failed status check"
        one = Data.objects.get(number="1")
        self.assertEqual(DataSerializer(one).data['status'], 'ok')