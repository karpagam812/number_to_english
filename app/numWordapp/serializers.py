from rest_framework import serializers
from .models import Data

class DataSerializer(serializers.ModelSerializer):
    num_in_english = serializers.SerializerMethodField('number_in_english')
    status = serializers.SerializerMethodField('status_check')
    def status_check(self,obj):
        try:
            i = int(obj.number)
            return "ok"
        except:
            return "Failed"

    def number_in_english(self, obj):
        ones = {
            0: '', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six',
            7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve',
            13: 'thirteen', 14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
            17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}

        thousands = {
            1: 'thousand', 2: 'million', 3: 'billion', 4: 'trillion', 5: 'quadrillion',
            6: 'quintillion', 7: 'sextillion', 8: 'septillion', 9: 'octillion',
            10: 'nonillion', 11: 'decillion'}

        tens = {
            2: 'twenty', 3: 'thirty', 4: 'forty', 5: 'fifty', 6: 'sixty',
            7: 'seventy', 8: 'eighty', 9: 'ninety'}
        
        illions = {
            1: 'thousand', 2: 'million', 3: 'billion', 4: 'trillion', 5: 'quadrillion',
            6: 'quintillion', 7: 'sextillion', 8: 'septillion', 9: 'octillion',
            10: 'nonillion', 11: 'decillion'}

        num = obj.number
        try:
            i = int(num)
            if i == 0:
                return 'zero'
        
            result = ''
            if i < 0:
                i = -1 * i
                result += 'Negative '
            
            stack = [(i, '')]  # Initialize stack with initial value and empty magnitude
            
            while stack:
                n, magnitude = stack.pop()
                
                if n < 20:
                    if magnitude:
                        result += ones[n] + ' ' + magnitude + ' '
                    else:
                        result += ones[n]
    
                elif n < 100:
                    if magnitude:
                        result += tens[n // 10] + ' ' + ones[n % 10] + ' ' + magnitude + ' '
                    else:
                        result += tens[n // 10] + ' ' + ones[n % 10]
                    
                elif n < 1000:
                    if magnitude:
                        stack.append((n % 100, magnitude))
                    else:
                        stack.append((n % 100, ''))
                    stack.append((n // 100, 'hundred'))
                
                else:
                    for illions_number, illions_name in illions.items():
                        if n < 1000**(illions_number + 1):
                            break
                    stack.append((n % (1000**illions_number), '' ))
                    stack.append((n // (1000**illions_number), illions_name))

            return result.strip()
        except:
            return "Not a number"

    class Meta:
        model=Data
        fields=('number','status','num_in_english')