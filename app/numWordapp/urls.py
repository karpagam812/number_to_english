from django.urls import path
from . import views
from django.conf import settings

urlpatterns = [
path('num_to_english/', views.postData),
path('num_to_english/get', views.getData),
]