from django.apps import AppConfig


class NumwordappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'numWordapp'
